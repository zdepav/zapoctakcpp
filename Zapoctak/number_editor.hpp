#pragma once
#include <string>
#include <ctime>
#include "font.hpp"
#include "screen_painter.hpp"

class number_editor {

    std::string number;

public:
    number_editor() : number("") { }

    std::string get_number() { return number; }

    void draw(const screen_painter<bool, 84, 48>& painter, font& f) {
        painter.text(1, 1, "NUMBER:", f, false);
        painter.text(3, 12, number, f, false);
        std::time_t t = std::time(nullptr);
        std::tm* time = std::localtime(&t);
        if (time->tm_sec % 2)
            painter.rect(2 + number.length() * 6, 11, 7, 9, false);
    }

    void send_key_press(const char button_id, const bool long_press) {
        switch (button_id) {
            case 'L':
                if (number.length() > 0) number.pop_back();
                break;
            case '0':
            case '1': case '2': case '3':
            case '4': case '5': case '6':
            case '7': case '8': case '9':
                if (number.length() < 9) number.push_back(button_id);
        }
    }

    void reset() { number = ""; }

    bool valid() { return number.length() == 9; }
};
