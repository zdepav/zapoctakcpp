#pragma once
#include <string>

class phone_os_api {
    
public:

    virtual ~phone_os_api() = default;

    virtual bool initiate_call(const std::string number) abstract = 0;

    virtual bool incoming_call(const std::string number) abstract = 0;

    virtual bool end_call() abstract = 0;

    virtual bool send_sms(const std::string number, const std::string text) abstract = 0;

    virtual bool recieve_sms(const std::string number, const std::string text) abstract = 0;
};