#pragma once
#include <string>
#include <vector>
#include "font.hpp"
#include "screen_painter.hpp"

class menu {

    std::vector<std::string> items;
    int selected_item_index, scroll;

    void fix_scroll() {
        if (items.size() > 2) {
            if (scroll > selected_item_index)
                scroll = selected_item_index;
            else if (scroll + 2 < selected_item_index)
                scroll = selected_item_index - 2;
            if (scroll > items.size() - 2)
                scroll = items.size() - 2;
        } else scroll = 0;
    }

public:
    explicit menu(const std::initializer_list<std::string> items)
        : items(items),
          selected_item_index(0),
          scroll(0) { }

    menu() : selected_item_index(0), scroll(0) { }

    void go_up() {
        if (items.empty()) return;
        if (--selected_item_index < 0)
            selected_item_index = items.size() - 1;
        fix_scroll();
    }

    void go_down() {
        if (items.empty()) return;
        if (++selected_item_index >= items.size())
            selected_item_index = 0;
        fix_scroll();
    }

    int get_selected_item_index() {
        return items.empty() ? -1 : selected_item_index;
    }

    void add_item(std::string item) {
        items.push_back(std::move(item));
    }

    void remove_selected_item() {
        if (items.empty()) return;
        items.erase(items.begin() + selected_item_index);
        --selected_item_index;
        fix_scroll();
    }

    void draw(const screen_painter<bool, 84, 48>& painter, font& f) {
        for (int i = scroll, j = 0; i < items.size() && i < scroll + 3; ++i, ++j) {
            if (i == selected_item_index) {
                painter.rect(0, j * 9, 84, 9, false);
                painter.text(1, j * 9 + 1, items[i], f, true);
            } else painter.text(1, j * 9 + 1, items[i], f, false);
        }
    }

    bool has_item() { return !items.empty(); }
};
