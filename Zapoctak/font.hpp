#pragma once
#include <array>
#include <unordered_map>
#include <fstream>

class font {
    
    std::unordered_map<char, std::array<bool, 35>> symbols;

public:

    explicit font(const char* filename) {
        std::ifstream font_file;
        font_file.open(filename);
        int ch;
        while ((ch = font_file.get()) == '\n' || ch == '\r') { }
        while (ch >= 0) {
            std::array<bool, 35> bmp;
            int c;
            for (int i = 0; i < 35; ++i) {
                while ((c = font_file.get()) == '\n' || c == '\r') { }
                bmp[i] = c == '#';
            }
            symbols[ch] = bmp;
            while ((ch = font_file.get()) == '\n' || ch == '\r') { }
        }
    }

    std::array<bool, 35>& get(const char ch) {
        const auto item = symbols.find(ch);
        if (item == symbols.end()) return symbols.at(' ');
        return item->second;
    }
};
