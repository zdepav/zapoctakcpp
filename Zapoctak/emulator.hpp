#pragma once
#include "allegro_wrapper.hpp"
#include "phone.hpp"

inline std::vector<std::string> split(std::string s, const char delim, const int max_c = 100) {
    std::vector<std::string> res;
    if (max_c > 0) {
        std::string buf;
        for (char c : s) {
            if (c == delim && res.size() < max_c - 1) {
                if (buf.length()) {
                    res.push_back(buf);
                    buf = "";
                }
            } else buf.push_back(c);
        }
        if (buf.length()) res.push_back(buf);
    }
    return res;
}

inline void convert(char& c) { c = toupper(static_cast<unsigned char>(c)); }


class emulator {

    shared_data* shd;
    ALLEGRO_DISPLAY* display;
    ALLEGRO_COLOR color_back;
    phone phone;
    ALLEGRO_THREAD* thread;

public:

    explicit emulator(ALLEGRO_THREAD* thread, shared_data* shd)
        : shd(shd),
          color_back(al_map_rgb(160, 160, 160)),
          phone(shd),
          thread(thread) {
        display = shd->allegro.create_display(206, 370);
        shd->allegro.create_timer(1.0 / 30.0, true);
        al_set_target_backbuffer(display);
        al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
        al_clear_to_color(color_back);
        al_flip_display();
    }

    void run() {
        bool redraw = true;
        while (!al_get_thread_should_stop(thread)) {
            const ALLEGRO_EVENT ev = shd->allegro.wait_for_event();
            if (ev.type == ALLEGRO_EVENT_TIMER) {
                phone.update();
                redraw = true;
            } else if (ev.type == ALLEGRO_EVENT_MOUSE_AXES || ev.type == ALLEGRO_EVENT_MOUSE_ENTER_DISPLAY) {
                phone.on_mouse_move(ev.mouse.x, ev.mouse.y);
            } else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_DOWN) {
                phone.on_mouse_down(ev.mouse.button);
            } else if (ev.type == ALLEGRO_EVENT_MOUSE_BUTTON_UP) {
                phone.on_mouse_up(ev.mouse.button);
            } else if (ev.type == ALLEGRO_EVENT_KEY_CHAR) {
                if (!ev.keyboard.repeat) phone.on_key_down(ev.keyboard.keycode, ev.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT);
            } else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
                break; // close the application
            }

            if (shd->has_waiting_command()) {
                std::vector<std::string> cmd = split(shd->dequeue_command(), ' ', 3);
                if (cmd[0] == "sms") {
                    for_each(cmd[2].begin(), cmd[2].end(), convert);
                    phone.recieve_sms(cmd[1], cmd[2]);
                } else if (cmd[0] == "call") {
                    phone.incoming_call(cmd[1]);
                } else if (cmd[0] == "endcall") {
                    phone.end_call();
                }
            }

            if (redraw && !shd->allegro.has_waiting_events()) {
                redraw = false;
                al_set_target_backbuffer(display);
                al_set_blender(ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
                al_clear_to_color(color_back);
                phone.draw(0, 0);
                al_flip_display();
            }
        }
        shd->end();
    }
};
