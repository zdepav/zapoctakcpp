#pragma once
#include <string>
#include <utility>
#include <ctime>
#include <cmath>

struct call {

    std::string number;
    bool incoming;
    clock_t start_time;

    explicit call(std::string number, const bool incoming = false)
        : number(std::move(number)),
          incoming(incoming),
          start_time(incoming ? 0 : std::clock()) { }

    call() : call("") { }

    call(const call& b) = default;

    call& operator=(const call& call) = default;

    void accept() {
        if (incoming) {
            incoming = false;
            start_time = std::clock();
        }
    }

    int time() { return incoming ? 0 : std::floor(double(std::clock() - start_time) / CLOCKS_PER_SEC); }
};
