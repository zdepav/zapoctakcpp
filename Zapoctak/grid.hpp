#pragma once
#include <array>

template<typename T, std::size_t WIDTH, std::size_t HEIGHT>
class grid {

    std::array<std::array<T, HEIGHT>, WIDTH> data;

public:

    void set(const std::size_t x, const std::size_t y, T value) {
        if (x >= WIDTH || y >= HEIGHT) throw std::out_of_range("");
        data[x][y] = value;
    }

    T get(const std::size_t x, const std::size_t y) {
        if (x >= WIDTH || y >= HEIGHT) throw std::out_of_range("");
        return data[x][y];
    }
};
