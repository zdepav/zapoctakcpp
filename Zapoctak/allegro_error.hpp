#pragma once
#include <stdexcept>

class allegro_error : public std::runtime_error {
public:
    explicit allegro_error(const char* message) : runtime_error(message) { }
};
