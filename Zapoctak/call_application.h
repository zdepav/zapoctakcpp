#pragma once
#include "application.hpp"

class call_application : public application {

    enum class states { menu, number, menu_in, menu_out };

    const std::string label_empty = "", label_DELETE = "DELETE", label_CALL = "CALL", label_SELECT = "SELECT", label_BACK = "BACK",
                      name_v = "CALL";

    states state;
    menu m, in_menu, out_menu;
    number_editor number_edit;

public:

    explicit call_application(phone_os_api* os_api)
        : application(os_api),
          state(states::menu),
          m({"CALL", "INCOMING", "OUTCOMING"}) {}

    std::string left_button_label() override {
        switch (state) {
            case states::number: return label_DELETE;
            case states::menu_in: return in_menu.has_item() ? label_DELETE : label_empty;
            case states::menu_out: return out_menu.has_item() ? label_DELETE : label_empty;
            default: return label_empty;
        }
    }

    std::string right_button_label() override {
        switch (state) {
            case states::number: return number_edit.valid() ? label_CALL : label_empty;
            case states::menu_in:
            case states::menu_out:
                return label_empty;
            default: return label_SELECT;
        }
    }

    const std::string& name() override { return name_v; }

    void draw(const screen_painter<bool, 84, 48>& painter, font& f) override {
        switch (state) {
            case states::menu:
                m.draw(painter, f);
                break;
            case states::number:
                number_edit.draw(painter, f);
                break;
            case states::menu_in:
                in_menu.draw(painter, f);
                break;
            case states::menu_out:
                out_menu.draw(painter, f);
                break;
        }
    }

    bool handle_back() override {
        switch (state) {
            case states::number:
            case states::menu_in:
            case states::menu_out:
                state = states::menu;
                return true;
            default: return false;
        }
    }

    void send_key_press(const char button_id, const bool long_press) override {
        switch (state) {
            case states::number:
                if (button_id == 'R' && number_edit.valid()) {
                    state = states::menu;
                    os_api->initiate_call(number_edit.get_number());
                } else number_edit.send_key_press(button_id, long_press);
                break;
            case states::menu:
                switch (button_id) {
                    case '^':
                        m.go_up();
                        break;
                    case 'v':
                        m.go_down();
                        break;
                    case 'R':
                        switch (m.get_selected_item_index()) {
                            case 0:
                                state = states::number;
                                break;
                            case 1:
                                state = states::menu_in;
                                break;
                            case 2:
                                state = states::menu_out;
                        }
                }
            case states::menu_in:
                switch (button_id) {
                    case '^':
                        in_menu.go_up();
                        break;
                    case 'v':
                        in_menu.go_down();
                        break;
                    case 'L':
                        if (in_menu.has_item())
                            in_menu.remove_selected_item();
                }
            case states::menu_out:
                switch (button_id) {
                    case '^':
                        out_menu.go_up();
                        break;
                    case 'v':
                        out_menu.go_down();
                        break;
                    case 'L':
                        if (out_menu.has_item())
                            out_menu.remove_selected_item();
                }
        }
    }

    void add_incoming_call(const std::string& number) {
        in_menu.add_item(number);
    }

    void add_outcoming_call(const std::string& number) {
        out_menu.add_item(number);
    }
};
