#pragma once
#include <vector>
#include <queue>
#include <algorithm>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include "allegro_error.hpp"

class shared_data;

class allegro_wrapper {

    std::vector<ALLEGRO_DISPLAY*> displays;
    std::vector<ALLEGRO_TIMER*> timers;
    std::vector<ALLEGRO_THREAD*> threads;
    ALLEGRO_EVENT_QUEUE* event_queue;

public:

    allegro_wrapper() : event_queue(nullptr) { }

    void init(const bool init_mouse, const bool init_keyboard) {
        if (!al_init()) throw allegro_error("Couldn't initialize allegro.");
        if (!al_init_primitives_addon()) throw allegro_error("Couldn't initialize allegro primitives addon.");
        if (!al_init_image_addon()) throw allegro_error("Couldn't initialize allegro image addon.");
        event_queue = al_create_event_queue();
        if (!event_queue) throw allegro_error("Couldn't initialize allegro event queue.");
        if (init_mouse) {
            if (!al_install_mouse()) throw allegro_error("Couldn't initialize mouse support.");
            al_register_event_source(event_queue, al_get_mouse_event_source());
        }
        if (init_keyboard) {
            if (!al_install_keyboard()) throw allegro_error("Couldn't initialize keyboard support.");
            al_register_event_source(event_queue, al_get_keyboard_event_source());
        }
    }

    ~allegro_wrapper() {
        for (auto display : displays) al_destroy_display(display);
        for (auto timer : timers) al_destroy_timer(timer);
        for (auto thread : threads) al_destroy_thread(thread);
        if (event_queue) al_destroy_event_queue(event_queue);
    }

    ALLEGRO_DISPLAY* create_display(const int width, const int height) {
        if (width < 64 || height < 64) throw std::out_of_range("Display size is too small.");
        ALLEGRO_DISPLAY* display = al_create_display(width, height);
        if (!display) throw allegro_error("Couldn't create display.");
        displays.push_back(display);
        al_register_event_source(event_queue, al_get_display_event_source(display));
        return display;
    }

    ALLEGRO_TIMER* create_timer(const double interval, const bool start = false) {
        ALLEGRO_TIMER* timer = al_create_timer(interval);
        if (!timer) throw allegro_error("Couldn't create timer.");
        timers.push_back(timer);
        al_register_event_source(event_queue, al_get_timer_event_source(timer));
        if (start) al_start_timer(timer);
        return timer;
    }

    ALLEGRO_THREAD* create_thread(void* (*f)(ALLEGRO_THREAD*, void*), shared_data* shd, const bool start = false) {
        ALLEGRO_THREAD* thread = al_create_thread(f, shd);
        if (!thread) throw allegro_error("Couldn't create thread.");
        threads.push_back(thread);
        if (start) al_start_thread(thread);
        return thread;
    }

    ALLEGRO_EVENT wait_for_event() const {
        ALLEGRO_EVENT ev;
        al_wait_for_event(event_queue, &ev);
        return ev;
    }

    bool has_waiting_events() const { return !al_is_event_queue_empty(event_queue); }

    void terminate_thread(ALLEGRO_THREAD* thread) {
        const auto position = std::find(threads.begin(), threads.end(), thread);
        if (position != threads.end()) threads.erase(position);
        al_destroy_thread(thread);
    }
};

class shared_data {

    ALLEGRO_MUTEX* mutex;
    std::queue<std::string> cli2emul;
    std::queue<std::string> emul2cli;

public:

    allegro_wrapper& allegro;
    bool ending;

    explicit shared_data(allegro_wrapper& allegro)
        : mutex(al_create_mutex()),
          allegro(allegro),
          ending(false) { }

    ~shared_data() {
        al_destroy_mutex(mutex);
    }

    void end() {
        al_lock_mutex(mutex);
        ending = true;
        al_unlock_mutex(mutex);
    }

    bool has_waiting_command() {
        al_lock_mutex(mutex);
        const auto sz = cli2emul.size();
        al_unlock_mutex(mutex);
        return sz > 0;
    }

    void enqueue_command(const std::string& command) {
        al_lock_mutex(mutex);
        cli2emul.push(command);
        al_unlock_mutex(mutex);
    }

    std::string dequeue_command() {
        al_lock_mutex(mutex);
        std::string cmd = cli2emul.front();
        cli2emul.pop();
        al_unlock_mutex(mutex);
        return cmd;
    }

    void lock_mutex() { al_lock_mutex(mutex); }

    void unlock_mutex() { al_unlock_mutex(mutex); }

    bool has_waiting_log_message() {
        al_lock_mutex(mutex);
        const auto sz = emul2cli.size();
        al_unlock_mutex(mutex);
        return sz > 0;
    }

    void enqueue_log(const std::string& message) {
        al_lock_mutex(mutex);
        emul2cli.push(message);
        al_unlock_mutex(mutex);
    }

    std::string dequeue_log() {
        al_lock_mutex(mutex);
        std::string message = emul2cli.front();
        emul2cli.pop();
        al_unlock_mutex(mutex);
        return message;
    }
};
