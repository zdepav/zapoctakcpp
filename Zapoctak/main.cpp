#include <iostream>
#include "phone.hpp"
#include "emulator.hpp"
#include "conio.h"
#include "allegro_wrapper.hpp"
#include "windows.h"

using namespace std;

void* run_emulation(ALLEGRO_THREAD* thread, void* arg) {
    auto* sd = static_cast<shared_data*>(arg);
    try {
        emulator e(thread, sd);
        e.run();
    } catch (exception& e) {
        sd->lock_mutex();
        cerr << e.what() << endl;
        sd->unlock_mutex();
    }
    return nullptr;
}

int run(allegro_wrapper& allegro, shared_data& shd) {
    ALLEGRO_THREAD* emulator_thread = nullptr;
    try {
        emulator_thread = allegro.create_thread(run_emulation, &shd, true);
        string line;
        while (!shd.ending) {
            while (shd.has_waiting_log_message()) {
                if (line.length() > 0) {
                    for (int i = 0; i < line.length(); ++i) cout << '\b';
                    for (int i = 0; i < line.length(); ++i) cout << ' ';
                    for (int i = 0; i < line.length(); ++i) cout << '\b';
                }
                cout << shd.dequeue_log();
                if (line.length() > 0) cout << line;
            }
            if (_kbhit()) {
                int ch = _getch();
                if (ch == 0 || ch == 0xE0) _getch();
                else {
                    if (ch == '\r' || ch == '\n') {
                        cout << '\n';
                        bool fail = false;
                        if (line == "exit") break;
                        if (line.length() > 14 && line.compare(0, 4, "sms ") == 0) {
                            for (int i = 4; i < 13; ++i) {
                                if (line[i] < '0' || line[i] > '9') {
                                    fail = true;
                                    break;
                                }
                            }
                            if (line[13] != ' ') fail = true;
                            if (!fail) {
                                shd.enqueue_command(line);
                                cout << "Sent" << '\n';
                            }
                        } else if (line.length() == 14 && line.compare(0, 5, "call ") == 0) {
                            for (int i = 5; i < 14; ++i) {
                                if (line[i] < '0' || line[i] > '9') {
                                    fail = true;
                                    break;
                                }
                            }
                            if (!fail) {
                                shd.enqueue_command(line);
                                cout << "Calling" << '\n';
                            }
                        } else if (line == "endcall") {
                            shd.enqueue_command(line);
                        } else fail = true;
                        if (fail) cout << "Invalid command!" << '\n';
                        line = "";
                    } else {
                        if (ch == '\b') {
                            // backspace -> remove last char (if there is one)
                            if (line.length() > 0) {
                                line.pop_back();
                                cout << "\b \b";
                            }
                        } else {
                            if (ch == '\t') ch = ' ';
                            if (line.length() < 70) {
                                cout << char(ch);
                                line.push_back(char(ch));
                            }
                        }
                    }
                }
            }
            al_rest(0.01);
        }
        allegro.terminate_thread(emulator_thread);
        while (!shd.ending) {}
        return 0;
    } catch (exception& e) {
        shd.lock_mutex();
        cerr << e.what() << endl;
        shd.unlock_mutex();
        if (emulator_thread) allegro.terminate_thread(emulator_thread);
        return 1;
    }
}

int main(int argc, char** argv) {
    auto con_handle = GetStdHandle(STD_OUTPUT_HANDLE);
    if (con_handle == INVALID_HANDLE_VALUE || con_handle == nullptr) {
        cerr << "Console window required!" << endl;
        return 1;
    }
    CONSOLE_SCREEN_BUFFER_INFO con_info;
    GetConsoleScreenBufferInfo(con_handle, &con_info);
    if (con_info.dwSize.X < 80) {
        COORD nsz;
        nsz.X = 80;
        nsz.Y = con_info.dwSize.Y;
        if (!SetConsoleScreenBufferSize(con_handle, nsz)) {
            cerr << "Console window is too small!" << endl;
            return 1;
        }
    }
    try {
        allegro_wrapper allegro;
        allegro.init(true, true);
        shared_data shd(allegro);
        return run(allegro, shd);
    } catch (exception& e) {
        cerr << e.what() << endl;
        return 1;
    }
}
