#pragma once
#include <string>
#include <unordered_map>
#include <ctime>
#include "font.hpp"
#include "screen_painter.hpp"

class text_editor {

    std::string text;
    const bool read_only;
    int cursor_pos, key_map_pos;
    std::clock_t last_key_press_time;
    char last_key_press_id;
    std::unordered_map<char, const char*> key_map;

    void move_up() {
        if (cursor_pos > 12) cursor_pos -= 13;
    }

    void move_down() {
        if (cursor_pos < text.length() - 12) cursor_pos += 13;
    }

    void move_left() {
        if (cursor_pos > 0) --cursor_pos;
    }

    void move_right() {
        if (cursor_pos < text.length()) ++cursor_pos;
    }

public:
    explicit text_editor(const bool read_only)
        : text(""),
          read_only(read_only),
          cursor_pos(0),
          key_map_pos(0),
          last_key_press_time(0),
          last_key_press_id('\0'),
          key_map{
              {'2', "ABC"},
              {'3', "DEF"},
              {'4', "GHI"},
              {'5', "JKL"},
              {'6', "MNO"},
              {'7', "PQRS"},
              {'8', "TUV"},
              {'9', "WXYZ"},
              {'*', ".,?!():+-*/%<>$=#"}
          } { }

    void set_text(const std::string text) {
        this->text = text.length() < 39 ? text : text.substr(0, 38);
        cursor_pos = this->text.length();
    }

    std::string get_text() { return text; }

    void draw(const screen_painter<bool, 84, 48>& painter, font& f) {
        std::time_t t = std::time(nullptr);
        std::tm* time = std::localtime(&t);
        int i;
        for (i = 0; i < text.length(); ++i) {
            if (i == cursor_pos && time->tm_sec % 2) {
                painter.rect(2 + (i % 13) * 6, (i / 13) * 9, 7, 9, false);
                painter.character(3 + (i % 13) * 6, 1 + (i / 13) * 9, text[i], f, true);
            } else painter.character(3 + (i % 13) * 6, 1 + (i / 13) * 9, text[i], f, false);
        }
        if (i == cursor_pos && time->tm_sec % 2)
            painter.rect(2 + (i % 13) * 6, (i / 13) * 9, 7, 9, false);
    }

    void send_key_press(const char button_id, const bool long_press) {
        switch (button_id) {
            case '<':
                move_left();
                break;
            case '>':
                move_right();
                break;
            case '^':
                move_up();
                break;
            case 'v':
                move_down();
                break;
            case 'L':
                if (read_only)break;
                if (cursor_pos > 0) {
                    text.erase(text.begin() + (cursor_pos - 1));
                    --cursor_pos;
                }
                break;
            case '0':
                if (read_only)break;
                if (text.length() < 38) {
                    if (long_press) text.insert(text.begin() + cursor_pos, '0');
                    else text.insert(text.begin() + cursor_pos, ' ');
                    ++cursor_pos;
                }
                break;
            case '2': case '3': case '4':
            case '5': case '6': case '7':
            case '8': case '9': case '*':
                if (read_only)break;
                if (long_press) {
                    if (text.length() < 38) text.insert(text.begin() + cursor_pos, button_id);
                    ++cursor_pos;
                } else {
                    if (button_id == last_key_press_id && double(std::clock() - last_key_press_time) / CLOCKS_PER_SEC < 1) {
                        if (text.length() <= 38) {
                            const char* chars = key_map[button_id];
                            if (chars[++key_map_pos] == '\0') key_map_pos = 0;
                            text[cursor_pos - 1] = chars[key_map_pos];
                        }
                    } else if (text.length() < 38) {
                        const char* chars = key_map[button_id];
                        key_map_pos = 0;
                        text.insert(text.begin() + cursor_pos, chars[0]);
                        ++cursor_pos;
                    }
                }
        }
        last_key_press_time = std::clock();
        last_key_press_id = long_press ? '\0' : button_id;
    }
};
