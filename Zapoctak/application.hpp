#pragma once
#include <string>
#include "font.hpp"
#include "phone_os_api.hpp"

class application {
protected:

    phone_os_api* os_api;
    
public:

    explicit application(phone_os_api* os_api) : os_api(os_api) { }

    virtual ~application() = default;

    virtual std::string left_button_label() abstract = 0;

    virtual std::string right_button_label() abstract = 0;
    
    virtual const std::string& name() abstract = 0;
    
    virtual void draw(const screen_painter<bool, 84, 48>& painter, font& f) abstract = 0;
    
    // returns false if app should be terminated
    virtual bool handle_back() abstract = 0;

    virtual void send_key_press(const char button_id, const bool long_press) abstract = 0;
};
