#pragma once
#include "application.hpp"
#include "menu.hpp"
#include "text_editor.hpp"
#include "number_editor.h"

class sms_application : public application {

    enum class states { menu, writing, number, menu_recieved, detail_recieved, menu_sent, detail_sent };

    const std::string label_empty = "", label_DELETE = "DELETE", label_SELECT = "SELECT", label_SEND = "SEND", label_BACK = "BACK", label_OK = "OK",
                      name_v = "SMS";

    std::vector<std::string> recieved, sent;
    std::vector<bool> recieved_read;
    states state;
    menu m, recieved_menu, sent_menu;
    text_editor message, detail;
    number_editor number_edit;

public:

    explicit sms_application(phone_os_api* os_api)
        : application(os_api),
          state(states::menu),
          m({"WRITE", "RECIEVED", "SENT"}),
          message(false),
          detail(true) { }

    std::string left_button_label() override {
        if (state == states::writing || state == states::number
            || (state == states::menu_recieved && !recieved.empty())
            || (state == states::menu_sent && !sent.empty())) {
            return label_DELETE;
        }
        return label_empty;
    }

    std::string right_button_label() override {
        switch (state) {
            case states::menu: return label_SELECT;
            case states::menu_recieved: return !recieved.empty() ? label_SELECT : label_empty;
            case states::menu_sent: return !sent.empty() ? label_SELECT : label_empty;
            case states::writing: return label_SEND;
            case states::number: return number_edit.valid() ? label_OK : label_empty;
            default: return label_BACK;
        }
    }

    const std::string& name() override { return name_v; }

    void draw(const screen_painter<bool, 84, 48>& painter, font& f) override {
        switch (state) {
            case states::menu:
                m.draw(painter, f);
                break;
            case states::writing:
                message.draw(painter, f);
                break;
            case states::number:
                number_edit.draw(painter, f);
                break;
            case states::menu_recieved:
                recieved_menu.draw(painter, f);
                break;
            case states::detail_recieved:
                detail.draw(painter, f);
                break;
            case states::menu_sent:
                sent_menu.draw(painter, f);
                break;
            case states::detail_sent:
                detail.draw(painter, f);
                break;
        }
    }

    bool handle_back() override {
        switch (state) {
            case states::writing:
                state = states::menu;
                return true;
            case states::number:
                state = states::writing;
                return true;
            case states::menu_recieved:
                state = states::menu;
                return true;
            case states::detail_recieved:
                state = states::menu_recieved;
                return true;
            case states::menu_sent:
                state = states::menu;
                return true;
            case states::detail_sent:
                state = states::menu_sent;
                return true;
            default: return false;
        }
    }

    void send_key_press(const char button_id, const bool long_press) override {
        switch (state) {
            case states::menu:
                switch (button_id) {
                    case '^':
                        m.go_up();
                        break;
                    case 'v':
                        m.go_down();
                        break;
                    case 'R':
                        switch (m.get_selected_item_index()) {
                            case 0:
                                state = states::writing;
                                break;
                            case 1:
                                state = states::menu_recieved;
                                break;
                            case 2:
                                state = states::menu_sent;
                        }
                }
                break;
            case states::writing:
                if (button_id == 'R' && message.get_text().length() > 0)
                    state = states::number;
                else message.send_key_press(button_id, long_press);
                break;
            case states::number:
                if (button_id == 'R' && number_edit.valid()) {
                    os_api->send_sms(number_edit.get_number(), message.get_text());
                    message.set_text("");
                    number_edit.reset();
                    state = states::menu;
                } else number_edit.send_key_press(button_id, long_press);
                break;
            case states::menu_recieved:
                switch (button_id) {
                    case '^':
                        recieved_menu.go_up();
                        break;
                    case 'v':
                        recieved_menu.go_down();
                        break;
                    case 'L':
                        if (!recieved.empty()) {
                            recieved.erase(recieved.begin() + recieved_menu.get_selected_item_index());
                            recieved_read.erase(recieved_read.begin() + recieved_menu.get_selected_item_index());
                            recieved_menu.remove_selected_item();
                        }
                        break;
                    case 'R':
                        if (!recieved.empty()) {
                            detail.set_text(recieved[recieved_menu.get_selected_item_index()]);
                            recieved_read[recieved_menu.get_selected_item_index()] = true;
                            state = states::detail_recieved;
                        }
                }
                break;
            case states::detail_recieved:
                if (button_id == 'R') state = states::menu_recieved;
                else detail.send_key_press(button_id, long_press);
                break;
            case states::menu_sent:
                switch (button_id) {
                    case '^':
                        sent_menu.go_up();
                        break;
                    case 'v':
                        sent_menu.go_down();
                        break;
                    case 'L':
                        if (!sent.empty()) {
                            sent.erase(sent.begin() + sent_menu.get_selected_item_index());
                            sent_menu.remove_selected_item();
                        }
                        break;
                    case 'R':
                        if (!sent.empty()) {
                            detail.set_text(sent[sent_menu.get_selected_item_index()]);
                            state = states::detail_sent;
                        }
                }
                break;
            case states::detail_sent:
                if (button_id == 'R') state = states::menu_sent;
                else detail.send_key_press(button_id, long_press);
        }
    }

    void add_sent_sms(const std::string& number, const std::string& text) {
        sent.push_back(text);
        sent_menu.add_item(number);
    }

    void add_recieved_sms(const std::string& number, const std::string& text) {
        recieved.push_back(text);
        recieved_read.push_back(false);
        recieved_menu.add_item(number);
    }

    bool has_unread_messages() {
        return std::find(recieved_read.begin(), recieved_read.end(), false) != recieved_read.end();
    }
};
