#pragma once
#include <cmath>
#include <fstream>
#include <filesystem>
#include <unordered_map>
#include "font.hpp"

template <typename T, std::size_t WIDTH, std::size_t HEIGHT>
class screen_painter {

    grid<T, WIDTH, HEIGHT>& screen;

    int area_x1, area_y1, area_x2, area_y2, area_w, area_h;

public:

    explicit screen_painter(grid<T, WIDTH, HEIGHT>& screen)
        : screen(screen),
          area_x1(0),
          area_y1(0),
          area_x2(WIDTH),
          area_y2(HEIGHT),
          area_w(WIDTH),
          area_h(HEIGHT) { }

    screen_painter(grid<T, WIDTH, HEIGHT>& screen, const int area_x, const int area_y, const int area_w, const int area_h)
        : screen(screen),
          area_x1(std::max(area_x, 0)),
          area_y1(std::max(area_y, 0)),
          area_x2(std::min(std::size_t(area_x + area_w), WIDTH)),
          area_y2(std::min(std::size_t(area_y + area_h), HEIGHT)),
          area_w(area_w),
          area_h(area_h) { }

    void clear(const T color) const {
        for (std::size_t x = area_x1; x < area_x2; ++x)
            for (std::size_t y = area_y1; y < area_y2; ++y)
                screen.set(x, y, color);
    }

    void pixel(const int x, const int y, const T color) const {
        if (x >= 0 && x < area_w && y >= 0 && y < area_h)
            screen.set(x + area_x1, y + area_y1, color);
    }

    void character(const int x, const int y, const char ch, font& f, const T color) const {
        const std::array<bool, 35>& bmp = f.get(ch);
        for (int i = 0; i < 35; ++i) {
            if (bmp[i]) pixel(x + i % 5, y + i / 5, color);
        }
    }

    void text(const int x, const int y, const char* s, font& f, const T color) const {
        int _x = x, _y = y;
        for (int i = 0; s[i] != '\0'; ++i) {
            if (s[i] == '\n') {
                _x = x;
                _y += 8;
            } else {
                character(_x, _y, s[i], f, color);
                _x += 6;
            }
        }
    }

    void text(const int x, const int y, const std::string s, font& f, const T color) const {
        int _x = x, _y = y;
        for (int i = 0; i < s.length(); ++i) {
            if (s[i] == '\n') {
                _x = x;
                _y += 8;
            } else {
                character(_x, _y, s[i], f, color);
                _x += 6;
            }
        }
    }

    void rect(int x, int y, int w, int h, const T color) const {
        if (w < 1 || h < 1) return;
        if (w == 1 && h == 1) pixel(x, y, color);
        if (x < 0) {
            if (-x >= w) return;
            w += x;
            x = 0;
        }
        if (x + w > area_w) {
            if (x >= area_w) return;
            w -= (x + w) - area_w;
        }
        if (y < 0) {
            if (-y >= h) return;
            h += y;
            y = 0;
        }
        if (y + h > area_h) {
            if (y >= area_h) return;
            h -= (y + h) - area_h;
        }
        x += area_x1;
        y += area_y1;
        for (std::size_t _x = 0; _x < w; ++_x)
            for (std::size_t _y = 0; _y < h; ++_y)
                screen.set(x + _x, y + _y, color);
    }

    void rect_outline(const int x, const int y, const int w, const int h, const T color) const {
        if (w < 1 || h < 1) return;
        if (w == 1 && h == 1) pixel(x, y, color);
        const int x2 = x + w - 1;
        const int y2 = y + h - 1;
        line(x, y, x2, y, color);
        if (h >= 2) line(x, y2, x2, y2, color);
        line(x, y, x, y2, color);
        if (w >= 2) line(x2, y, x2, y2, color);
    }

    void line(const int x1, const int y1, const int x2, const int y2, const T color) const {
        if (x1 == x2) {
            if (y1 == y2) pixel(x1, y1, color);
            else {
                const auto yt = std::max(y1, y2);
                for (auto y = std::min(y1, y2); y <= yt; ++y)
                    pixel(x1, y, color);
            }
        } else {
            if (y1 == y2) {
                const auto xt = std::max(x1, x2);
                for (auto x = std::min(x1, x2); x <= xt; ++x)
                    pixel(x, y1, color);
            } else {
                const auto xd = x2 - x1, yd = y2 - y1;
                if (std::abs(xd) > std::abs(yd)) {
                    const double vy = double(yd) / xd;
                    const int vx = xd > 0 ? 1 : -1;
                    double y = y1;
                    int x;
                    for (x = x1; x != x2; x += vx, y += vy)
                        pixel(x, std::round(y), color);
                    pixel(x, std::round(y), color);
                } else {
                    const double vx = double(xd) / yd;
                    const int vy = yd > 0 ? 1 : -1;
                    double x = x1;
                    int y;
                    for (y = y1; y != y2; x += vx, y += vy)
                        pixel(std::round(x), y, color);
                    pixel(std::round(x), y, color);
                }
            }
        }
    }
};
