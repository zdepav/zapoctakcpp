﻿#pragma once
#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <ctime>
#include "grid.hpp"
#include "phone_os.hpp"
#include "allegro_error.hpp"
#include "allegro_wrapper.hpp"

struct phone_button {

    short x1, x2, y1, y2;

    char id;

    phone_button(const char id, const short x, const short y, const short w, const short h)
        : x1(x), x2(x + w), y1(y), y2(y + h), id(id) { }

    phone_button(const phone_button& b) = default;

    phone_button& operator=(const phone_button& b) = default;

    bool pressed(const int x, const int y) const {
        return x >= x1 && x < x2 && y >= y1 && y < y2;
    }

    bool operator==(const phone_button& o) const { return id == o.id; }

    bool operator!=(const phone_button& o) const { return id != o.id; }
};

class phone : phone_os_api {

    grid<bool, 84, 48> screen;
    const ALLEGRO_COLOR color_pixel_on, color_pixel_off, color_screen_back, color_button_highlight;
    const int screen_offset_x = 19, screen_offset_y = 35;
    ALLEGRO_BITMAP* image;
    /* ┌─────────┐
       │ ███████ │
       │ ███████ │
       │ L     R │
       │ C  A  E │
       │ 1  2  3 │
       │ 4  5  6 │
       │ 7  8  9 │
       │ *  0  # │
       └─────────┘ */
    std::array<phone_button, 17> buttons;
    phone_button button_up, button_down, button_left, button_right, button_none;

    int mouse_x, mouse_y;
    clock_t last_mouse_down;
    phone_button pressed_button;
    phone_os os;

public:

    explicit phone(shared_data *shd)
        : color_pixel_on(al_map_rgb(210, 236, 121)),
          color_pixel_off(al_map_rgb(73, 85, 29)),
          color_screen_back(al_map_rgba(33, 38, 13, 85)),
          color_button_highlight(al_map_rgba(64, 64, 64, 85)),
          image(al_load_bitmap("phone.tga")),
          buttons{
              phone_button('L', 19, 146, 50, 24),
              phone_button('R', 137, 146, 50, 24),
              phone_button('C', 19, 181, 50, 24),
              phone_button('A', 78, 155, 50, 50),
              phone_button('E', 137, 181, 50, 24),
              phone_button('1', 19, 216, 50, 24),
              phone_button('2', 78, 216, 50, 24),
              phone_button('3', 137, 216, 50, 24),
              phone_button('4', 19, 251, 50, 24),
              phone_button('5', 78, 251, 50, 24),
              phone_button('6', 137, 251, 50, 24),
              phone_button('7', 19, 286, 50, 24),
              phone_button('8', 78, 286, 50, 24),
              phone_button('9', 137, 286, 50, 24),
              phone_button('*', 19, 321, 50, 24),
              phone_button('0', 78, 321, 50, 24),
              phone_button('#', 137, 321, 50, 24)
          },
          button_up('^', 78, 155, 50, 50),
          button_down('v', 78, 155, 50, 50),
          button_left('<', 78, 155, 50, 50),
          button_right('>', 78, 155, 50, 50),
          button_none('\0', 0, 0, 0, 0),
          mouse_x(0),
          mouse_y(0),
          last_mouse_down(0),
          pressed_button(button_none),
          os(screen, shd) {
        if (!image) throw allegro_error("Couldn't load image 'phone.tga'.");
    }

    ~phone() { al_destroy_bitmap(image); }

    void update() {
        if (pressed_button != button_none && double(std::clock() - last_mouse_down) / CLOCKS_PER_SEC > 1.5) {
            os.send_key_press(pressed_button.id, true);
            pressed_button = button_none;
        }
        os.update();
    }

    void draw(const int x, const int y) {
        al_draw_bitmap(image, x, y, 0);
        if (os.is_screen_on()) {
            for (std::size_t _y = 0; _y < 48; ++_y) {
                std::size_t l = 0, r = 0;
                bool cp = screen.get(0, _y);
                for (std::size_t _x = 0; _x < 84; ++_x) {
                    const bool c = screen.get(_x, _y);
                    if (c != cp) {
                        al_draw_filled_rectangle(
                            l + screen_offset_x + x,
                            _y * 2 + screen_offset_y + y,
                            r + screen_offset_x + x,
                            _y * 2 + screen_offset_y + 2 + y,
                            cp ? color_pixel_on : color_pixel_off);
                        l = r;
                        cp = c;
                    }
                    r += 2;
                }
                al_draw_filled_rectangle(
                    l + screen_offset_x + x,
                    _y * 2 + screen_offset_y + y,
                    r + screen_offset_x + x,
                    _y * 2 + screen_offset_y + 2 + y,
                    cp ? color_pixel_on : color_pixel_off);
            }
            al_draw_rectangle(
                screen_offset_x + x + 1,
                screen_offset_y + y + 1,
                screen_offset_x + x + 168,
                screen_offset_y + y + 96,
                color_screen_back, 1);
        }
        if (pressed_button != button_none) {
            al_draw_filled_rectangle(
                pressed_button.x1,
                pressed_button.y1,
                pressed_button.x2 - 1,
                pressed_button.y2 - 1,
                color_button_highlight);
        }
    }

    phone_button get_button(int x, int y) {
        for (auto& b : buttons) {
            if (b.pressed(x, y)) {
                if (b.id == 'A') {
                    // arrows
                    x -= b.x1;
                    y -= b.y1;
                    return x + y < 50 ? (x > y ? button_up : button_left) : (x > y ? button_right : button_down);
                }
                return b;
            }
        }
        return button_none;
    }

    void on_mouse_move(const int x, const int y) {
        mouse_x = x;
        mouse_y = y;
        const auto b = get_button(mouse_x, mouse_y);
        if (pressed_button != button_none && b != pressed_button) {
            pressed_button = button_none;
        }
    }

    void on_mouse_down(const unsigned mouse_button) {
        if (mouse_button == 1) {
            last_mouse_down = std::clock();
            pressed_button = get_button(mouse_x, mouse_y);
        }
    }

    void on_mouse_up(const unsigned mouse_button) {
        if (mouse_button == 1) {
            const auto b = get_button(mouse_x, mouse_y);
            if (pressed_button != button_none && b == pressed_button)
                os.send_key_press(pressed_button.id, false);
            pressed_button = button_none;
        }
    }

    void on_key_down(const int keycode, const bool shift) {
        switch (keycode) {
            case ALLEGRO_KEY_PAD_0:
                os.send_key_press('0', shift);
                break;
            case ALLEGRO_KEY_PAD_1:
                os.send_key_press('1', shift);
                break;
            case ALLEGRO_KEY_PAD_2:
                os.send_key_press('2', shift);
                break;
            case ALLEGRO_KEY_PAD_3:
                os.send_key_press('3', shift);
                break;
            case ALLEGRO_KEY_PAD_4:
                os.send_key_press('4', shift);
                break;
            case ALLEGRO_KEY_PAD_5:
                os.send_key_press('5', shift);
                break;
            case ALLEGRO_KEY_PAD_6:
                os.send_key_press('6', shift);
                break;
            case ALLEGRO_KEY_PAD_7:
                os.send_key_press('7', shift);
                break;
            case ALLEGRO_KEY_PAD_8:
                os.send_key_press('8', shift);
                break;
            case ALLEGRO_KEY_PAD_9:
                os.send_key_press('9', shift);
                break;
            case ALLEGRO_KEY_PAD_SLASH:
                os.send_key_press('#', shift);
                break;
            case ALLEGRO_KEY_PAD_ASTERISK:
                os.send_key_press('*', shift);
                break;
            case ALLEGRO_KEY_UP:
                os.send_key_press('^', shift);
                break;
            case ALLEGRO_KEY_DOWN:
                os.send_key_press('v', shift);
                break;
            case ALLEGRO_KEY_LEFT:
                os.send_key_press('<', shift);
                break;
            case ALLEGRO_KEY_RIGHT:
                os.send_key_press('>', shift);
                break;
            case ALLEGRO_KEY_ESCAPE:
                os.send_key_press('E', shift);
                break;
            case ALLEGRO_KEY_ENTER:
            case ALLEGRO_KEY_PAD_ENTER:
                os.send_key_press('R', shift);
                break;
            case ALLEGRO_KEY_BACKSPACE:
                os.send_key_press('L', shift);
                break;
            case ALLEGRO_KEY_SPACE:
                os.send_key_press('S', shift);
                break;
        }
    }

    // API

    bool initiate_call(const std::string number) override { return os.initiate_call(number); }

    bool incoming_call(const std::string number) override { return os.incoming_call(number); }

    bool end_call() override { return os.end_call(); }

    bool send_sms(const std::string number, const std::string text) override { return os.send_sms(number, text); }

    bool recieve_sms(const std::string number, const std::string text) override { return os.recieve_sms(number, text); }
};
