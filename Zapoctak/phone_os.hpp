#pragma once
#pragma warning(disable:4996) // otherwise VS doesn't like usage of localtime and keeps suggesting an alternative that doesn't exist in C++ (exists in "modern" C)
#include <ctime>
#include "screen_painter.hpp"
#include "application.hpp"
#include "sms_application.hpp"
#include "call.hpp"
#include "phone_os_api.hpp"
#include "call_application.h"
#include "allegro_wrapper.hpp"

class phone_os : public phone_os_api {

    using screen_t = grid<bool, 84, 48>;
    using painter_t = screen_painter<bool, 84, 48>;

    bool on, high_signal, calling;
    int turning_on_off_frame; // > 0 => animace b��
    std::string left_button_label, right_button_label;
    font f;
    painter_t painter, app_painter;
    std::array<std::unique_ptr<application>, 2> apps;
    menu app_menu;
    application* current_app;
    call current_call;
    shared_data *shd;

public:

    explicit phone_os(screen_t& screen, shared_data *shd)
        : on(false),
          high_signal(true),
          calling(false),
          turning_on_off_frame(0),
          f("font.dat"),
          painter(screen),
          app_painter(screen, 0, 10, 84, 30),
          apps{
              std::make_unique<call_application>(this),
              std::make_unique<sms_application>(this)
          },
          app_menu({"CALL", "SMS"}),
          current_app(nullptr),
          shd(shd) { }

    bool is_screen_on() const { return on || turning_on_off_frame; }

    bool is_calling() { return calling; }

    void paint_on_off_anim() const {
        painter.clear(false);
        painter.rect(0, 48 - turning_on_off_frame, 84, turning_on_off_frame, true);
        painter.line(41, 17, 42, 17, false);
        painter.line(39, 18, 44, 18, false);
        painter.line(37, 19, 46, 19, false);
        painter.line(35, 20, 48, 20, false);
        painter.line(33, 21, 50, 21, false);
        painter.line(36, 22, 47, 22, false);
        painter.line(37, 23, 46, 23, false);
        painter.line(37, 24, 46, 24, false);
        painter.line(36, 25, 47, 25, false);
        painter.line(33, 26, 50, 26, false);
        painter.line(35, 27, 48, 27, false);
        painter.line(37, 28, 46, 28, false);
        painter.line(39, 29, 44, 29, false);
        painter.line(41, 30, 42, 30, false);
    }

    void paint_top_bar(std::tm* time) {
        //
        // TIME
        //
        painter.character(1, 1, '0' + (time->tm_hour / 10), f, false);
        painter.character(7, 1, '0' + (time->tm_hour % 10), f, false);
        if (time->tm_sec % 2) painter.character(11, 1, ':', f, false);
        painter.character(15, 1, '0' + (time->tm_min / 10), f, false);
        painter.character(21, 1, '0' + (time->tm_min % 10), f, false);
        if (dynamic_cast<sms_application*>(apps[1].get())->has_unread_messages()) {
            //
            // UNREAD MESSAGE
            //
            painter.rect_outline(37, 1, 10, 7, false);
            painter.line(38, 2, 41, 5, false);
            painter.line(45, 2, 42, 5, false);
            painter.pixel(38, 6, false);
            painter.pixel(39, 5, false);
            painter.pixel(45, 6, false);
            painter.pixel(44, 5, false);
        }
        //
        // BATTERY
        //
        painter.line(54, 3, 54, 5, false);
        painter.rect_outline(55, 1, 15, 7, false);
        painter.rect(57, 3, 2, 3, false);
        painter.rect(60, 3, 2, 3, false);
        painter.rect(63, 3, 2, 3, false);
        painter.rect(66, 3, 2, 3, false);
        //
        // SIGNAL
        //
        painter.line(72, 7, 73, 7, false);
        painter.rect(75, 5, 2, 3, false);
        painter.rect(78, 3, 2, 5, false);
        if (high_signal) painter.rect(81, 1, 2, 7, false);
        //
        // BOTTOM LINE
        //
        painter.line(0, 9, 83, 9, false);
    }

    void paint_bottom_bar(const bool empty = false) {
        //
        // TOP LINE, CENTER VERTICAL LINE
        //
        painter.line(0, 38, 83, 38, false);
        painter.rect(41, 39, 2, 9, false);
        if (!empty) {
            //
            // BUTTON LABELS
            //
            if (current_app != nullptr) {
                painter.text(1, 40, current_app->left_button_label(), f, false);
                painter.text(44, 40, current_app->right_button_label(), f, false);
            } else painter.text(44, 40, "SELECT", f, false);
        }
    }

    void update() {
        if (turning_on_off_frame) {
            paint_on_off_anim();
            if (on) {
                if (++turning_on_off_frame == 49)
                    turning_on_off_frame = 0;
            } else --turning_on_off_frame;
        } else {
            if (!on) return;
            painter.clear(true);
            if (rand() % 300 == 0) high_signal = !high_signal;
            std::time_t t = std::time(nullptr);
            std::tm* time = std::localtime(&t);
            paint_top_bar(time);
            if (calling) {
                paint_bottom_bar(true);
                painter.text(13, 15, current_call.number.substr(0, 3), f, false);
                painter.text(33, 15, current_call.number.substr(3, 3), f, false);
                painter.text(53, 15, current_call.number.substr(6, 3), f, false);
                if (current_call.incoming) {
                    if (time->tm_sec % 2) {
                        painter.line(50, 25, 55, 25, false);
                        painter.line(44, 26, 56, 26, false);
                        painter.line(38, 27, 56, 27, false);
                        painter.line(32, 28, 44, 28, false);
                        painter.line(27, 29, 38, 29, false);
                        painter.rect(26, 30, 7, 4, false);
                        painter.rect(50, 28, 7, 2, false);
                    } else {
                        painter.line(27, 25, 32, 25, false);
                        painter.line(26, 26, 38, 26, false);
                        painter.line(26, 27, 44, 27, false);
                        painter.line(38, 28, 50, 28, false);
                        painter.line(44, 29, 55, 29, false);
                        painter.rect(26, 28, 7, 2, false);
                        painter.rect(50, 30, 7, 4, false);
                    }
                } else {
                    const int tm = current_call.time();
                    painter.character(22, 26, '0' + (tm / 360000), f, false);
                    painter.character(28, 26, '0' + (tm / 36000 % 10), f, false);
                    painter.character(32, 26, ':', f, false);
                    painter.character(36, 26, '0' + (tm / 600), f, false);
                    painter.character(42, 26, '0' + (tm / 60 % 10), f, false);
                    painter.character(46, 26, ':', f, false);
                    painter.character(50, 26, '0' + (tm / 10), f, false);
                    painter.character(56, 26, '0' + (tm % 10), f, false);
                }
            } else {
                paint_bottom_bar();
                if (current_app != nullptr)
                    current_app->draw(app_painter, f);
                else {
                    // menu
                    app_menu.draw(app_painter, f);
                }
            }
        }
    }

    void send_key_press(const char button_id, const bool long_press) {
        if (turning_on_off_frame) return;
        if (long_press) {
            if (button_id == 'E') {
                if (on) {
                    turning_on_off_frame = 48;
                    on = false;
                    calling = false;
                } else {
                    turning_on_off_frame = 1;
                    on = true;
                }
                return;
            }
            if (button_id == '#' && !calling) {
                if (current_app != nullptr) {
                    while (current_app->handle_back()) { }
                    current_app = nullptr;
                }
                return;
            }
        }
        if (!on) return;
        if (calling) {
            if (button_id == 'E') calling = false;
            else if (button_id == 'S' && current_call.incoming) current_call.accept();
        } else {
            if (current_app != nullptr) {
                if (button_id == '#') {
                    if (!current_app->handle_back()) {
                        current_app = nullptr;
                    }
                } else current_app->send_key_press(button_id, long_press);
            } else {
                if (button_id == 'R') current_app = apps[app_menu.get_selected_item_index()].get();
                else if (button_id == '^') app_menu.go_up();
                else if (button_id == 'v') app_menu.go_down();
            }
        }
    }

    // API

    bool initiate_call(const std::string number) override {
        if (calling || !on) return false;
        calling = true;
        current_call = call(number);
        dynamic_cast<call_application*>(apps[0].get())->add_outcoming_call(number);
        shd->enqueue_log("Calling from phone simulator to " + number + '\n');
        return true;
    }

    bool incoming_call(const std::string number) override {
        if (calling || !on) return false;
        calling = true;
        current_call = call(number, true);
        dynamic_cast<call_application*>(apps[0].get())->add_incoming_call(number);
        return true;
    }

    bool end_call() override {
        const bool ret = calling && on;
        calling = false;
        return ret;
    }

    bool send_sms(const std::string number, const std::string text) override {
        if (!on) return false;
        dynamic_cast<sms_application*>(apps[1].get())->add_sent_sms(number, text);
        shd->enqueue_log("SMS sent from phone simulator to " + number + '\n' + text + '\n');
        return true;
    }

    bool recieve_sms(const std::string number, const std::string text) override {
        dynamic_cast<sms_application*>(apps[1].get())->add_recieved_sms(number, text);
        return true;
    }
};
